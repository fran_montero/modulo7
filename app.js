console.log("XXXXXXXXXXXXX");
console.log("EJERCICIO 1");
console.log("XXXXXXXXXXXXX");

let ej1caso1 = "ES6600190020961234567890";
let ej1caso2 = "ES66 0019 0020 9612 3456 7890";

let ej1regex1 = /[A-Z]{2}\d{22}$/;
let ej1regex2 = /[A-Z]{2}\d{2}\s*\d{4}\s*\d{4}\s*\d{4}\s*\d{4}\s*\d{4}$/
let ej1regex3 = /([A-Z]{2})(\d{2})\s*\d{4}\s*\d{4}\s*\d{4}\s*\d{4}\s*\d{4}$/


console.log("CASO 1: ");
console.log("¿Es válido " + ej1caso1 + "?: " + ej1regex1.test(ej1caso1));

console.log("CASO 2: ");
console.log("¿Es válido " + ej1caso1 + "?: " + ej1regex2.test(ej1caso1));
console.log("¿Es válido " + ej1caso2 + "?: " + ej1regex2.test(ej1caso2));

console.log("CASO 3: ");
console.log("Código del país: " + ej1caso1.match(ej1regex3)[1]);
console.log("Dígito de control: " + ej1caso1.match(ej1regex3)[2]);



console.log("XXXXXXXXXXXXX");
console.log("EJERCICIO 2");
console.log("XXXXXXXXXXXXX");

let ej2caso1 = Array("2021 GMD", "2345-GMD", "4532BDB", "0320-AAA");
let ej2regex1 = /(\d{4})(\s|-)?([A-Z]{3})$/;

for (let i = 0; i < ej2caso1.length; i++) {
    console.log("¿La matrícula " + ej2caso1[i] + " es válida?: " + ej2regex1.test(ej2caso1[i]));
    if (ej2regex1.test(ej2caso1[i])) {
        console.log("  Número: " + ej2caso1[i].match(ej2regex1)[1]);
        console.log("  Letras: " + ej2caso1[i].match(ej2regex1)[3]);
    }
}
